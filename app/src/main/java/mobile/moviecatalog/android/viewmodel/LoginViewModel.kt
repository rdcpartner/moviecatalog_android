package mobile.moviecatalog.android.viewmodel

import android.util.Log
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import mobile.moviecatalog.android.App
import mobile.moviecatalog.android.Constants
import mobile.moviecatalog.android.model.ApiError
import mobile.moviecatalog.android.model.Configuration
import mobile.moviecatalog.android.model.LoginRequest
import mobile.moviecatalog.android.model.RequestTokenResponse
import mobile.moviecatalog.android.service.AuthApi
import mobile.moviecatalog.android.service.RetrofitInstance
import retrofit2.HttpException

class LoginViewModel : ViewModel() {

    private var apiService = RetrofitInstance.getRetrofit().create(AuthApi::class.java)
    val user = MutableLiveData<LoginRequest>()
    val isError = MutableLiveData<Boolean>()
    val errorMessage = MutableLiveData<String>()
    private var compositeDisposable = CompositeDisposable()





    fun login(user:LoginRequest) {
        compositeDisposable.add(
            apiService.createRequestToken(Constants.API_KEY).subscribeOn(Schedulers.newThread())
                .observeOn(
                    AndroidSchedulers.mainThread()
                ).
                subscribeWith(object :
                    DisposableSingleObserver<RequestTokenResponse>() {
                    override fun onSuccess(response: RequestTokenResponse) {
                        if (!response.request_token.isNullOrEmpty()) {

                                 user.request_token=response.request_token
                            loginService(user)
                        }

                    }
                    override fun onError(e: Throwable) {
                        Log.i("RequestError", e.message.toString())
                    }

                })
        )
    }

    fun loginService(loginRequest: LoginRequest) {
        compositeDisposable.add(
            apiService.login(
                Constants.API_KEY,
                loginRequest.username!!,
                loginRequest.password!!,
                loginRequest.request_token!!
            ).subscribeOn(Schedulers.newThread()).observeOn(
                AndroidSchedulers.mainThread()
            ).subscribeWith(object :
                DisposableSingleObserver<RequestTokenResponse>() {
                override fun onSuccess(response: RequestTokenResponse) {
                    user.value = loginRequest
                    App.requestToken=response.request_token
                    configService()
                    isError.value = false

                }
                override fun onError(e: Throwable) {
                    val errorBody =
                        (e as HttpException).response().errorBody()?.source()?.buffer()?.clone()
                            ?.readUtf8()
                    val body = Gson().fromJson(errorBody, ApiError::class.java)
                    isError.value = true
                    errorMessage.value = body.status_message.toString()
                }

            })
        )
    }



    fun configService() {
        compositeDisposable.add(
            apiService.getConfigs(
                Constants.API_KEY,

            ).subscribeOn(Schedulers.newThread()).observeOn(
                AndroidSchedulers.mainThread()
            ).subscribeWith(object :
                DisposableSingleObserver<Configuration>() {
                override fun onSuccess(response: Configuration) {
                     App.configuration=response
                }
                override fun onError(e: Throwable) {
                    val errorBody =
                        (e as HttpException).response().errorBody()?.source()?.buffer()?.clone()
                            ?.readUtf8()
                    val body = Gson().fromJson(errorBody, ApiError::class.java)
                    isError.value = true
                    errorMessage.value = body.status_message.toString()
                }

            })
        )


    }





}