package mobile.moviecatalog.android.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import mobile.moviecatalog.android.App
import mobile.moviecatalog.android.Constants
import mobile.moviecatalog.android.model.*
import mobile.moviecatalog.android.service.RetrofitInstance
import mobile.moviecatalog.android.service.WatchlistApi
import retrofit2.HttpException


class WatchlistViewModel: ViewModel() {

    private var apiService = RetrofitInstance.getRetrofit().create(WatchlistApi::class.java)
    var isError = MutableLiveData<Boolean>()
    var loading = MutableLiveData<Boolean>()
    private var compositeDisposable = CompositeDisposable()
    var  responseWatchList= MutableLiveData<MovieModel>()
    var  responseFavList= MutableLiveData<MovieModel>()
    private lateinit var type:String

    init {
        loading.value=true
    }


      fun setTypeForService(value:String){
        type=value
    }



    fun getSessionID(){
        compositeDisposable.add(
            apiService.getSessionToken(Constants.API_KEY, App.requestToken!!).subscribeOn(Schedulers.newThread())
                .observeOn(
                    AndroidSchedulers.mainThread()
                ).
                subscribeWith(object :
                    DisposableSingleObserver<SessionMoodel>() {
                    override fun onSuccess(response: SessionMoodel) {
                        if (!response.sessionId.isNullOrEmpty()) {
                            App.sessionId=response.sessionId
                        getAccountId()

                        }
                    }
                    override fun onError(e: Throwable) {

                        val errorBody =
                            (e as HttpException).response().errorBody()?.source()?.buffer()?.clone()
                                ?.readUtf8()
                        val body = Gson().fromJson(errorBody, ApiError::class.java)
                        isError.value = true


                        Log.i("RequestError",   body.status_message.toString())
                    }

                })
        )

    }

   fun getAccountId(){
       compositeDisposable.add(
           apiService.getAccountId(Constants.API_KEY, App.sessionId!!).subscribeOn(Schedulers.newThread())
               .observeOn(
                   AndroidSchedulers.mainThread()
               ).
               subscribeWith(object :
                   DisposableSingleObserver<AccountModel>() {
                   override fun onSuccess(response: AccountModel) {
                       if (response.id!=null) {
                           App.accountId=response.id
                           if(type.equals(Constants.KEY_WATCHLIST))
                            getWatchList()
                           else
                               getFavList()
                       }
                   }
                   override fun onError(e: Throwable) {
                       val errorBody =
                           (e as HttpException).response().errorBody()?.source()?.buffer()?.clone()
                               ?.readUtf8()
                       val body = Gson().fromJson(errorBody, ApiError::class.java)
                       isError.value = true

                       Log.i("RequestError",   body.status_message.toString())
                   }

               })
       )

   }





    fun getWatchList(){

        compositeDisposable.add(
            apiService.getWatchlist(App.accountId!!,Constants.API_KEY, App.sessionId!!).subscribeOn(Schedulers.newThread())
                .observeOn(
                    AndroidSchedulers.mainThread()
                ).
                subscribeWith(object :
                    DisposableSingleObserver<MovieModel>() {
                    override fun onSuccess(response: MovieModel) {
                            responseWatchList.value=response
                        loading.value=false
                    }
                    override fun onError(e: Throwable) {
                        loading.value=true
                        val errorBody =
                            (e as HttpException).response().errorBody()?.source()?.buffer()?.clone()
                                ?.readUtf8()
                        val body = Gson().fromJson(errorBody, ApiError::class.java)
                        isError.value = true
                        Log.i("RequestError",   body.status_message.toString())
                    }
                })
        )


    }

    fun getFavList(){

        compositeDisposable.add(
            apiService.getFavorites(App.accountId!!,Constants.API_KEY, App.sessionId!!).subscribeOn(Schedulers.newThread())
                .observeOn(
                    AndroidSchedulers.mainThread()
                ).
                subscribeWith(object :
                    DisposableSingleObserver<MovieModel>() {
                    override fun onSuccess(response: MovieModel) {

                            responseFavList.value=response
                        loading.value=false


                    }
                    override fun onError(e: Throwable) {
                        val errorBody =
                            (e as HttpException).response().errorBody()?.source()?.buffer()?.clone()
                                ?.readUtf8()
                        val body = Gson().fromJson(errorBody, ApiError::class.java)
                        isError.value = true
                        loading.value=true

                        Log.i("RequestError",   body.status_message.toString())
                    }

                })
        )


    }

}