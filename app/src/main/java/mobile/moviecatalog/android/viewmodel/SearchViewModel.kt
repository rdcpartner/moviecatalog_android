package mobile.moviecatalog.android.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import mobile.moviecatalog.android.Constants
import mobile.moviecatalog.android.model.MovieModel
import mobile.moviecatalog.android.service.RetrofitInstance
import mobile.moviecatalog.android.service.SearchApi

class SearchViewModel : ViewModel() {

    private var apiService = RetrofitInstance.getRetrofit().create(SearchApi::class.java)
    var isError = MutableLiveData<Boolean>()
    var loading = MutableLiveData<Boolean>()
    private var compositeDisposable = CompositeDisposable()
    var responseMovieModel = MutableLiveData<MovieModel>()


    fun movieService(value: Int?) {
        loading.value = true
        if (value != null)
            getAllMoviesByPageService(value)
        else
            getAllMoviesService()

    }

    fun getAllMoviesService() {
        compositeDisposable.add(
            apiService.getAllMovies(Constants.API_KEY).subscribeOn(Schedulers.newThread())
                .observeOn(
                    AndroidSchedulers.mainThread()
                ).subscribeWith(object :
                    DisposableSingleObserver<MovieModel>() {
                    override fun onSuccess(t: MovieModel) {
                        isError.value = false
                        responseMovieModel.value = t
                        loading.value = false
                    }

                    override fun onError(e: Throwable) {
                        isError.value = true
                        loading.value = true
                    }
                })
        )
    }

    fun getAllMoviesByPageService(page: Int) {
        compositeDisposable.add(
            apiService.getMoviesByPage(Constants.API_KEY, page).subscribeOn(Schedulers.newThread())
                .observeOn(
                    AndroidSchedulers.mainThread()
                ).subscribeWith(object :
                    DisposableSingleObserver<MovieModel>() {
                    override fun onSuccess(t: MovieModel) {
                        isError.value = false
                        responseMovieModel.value = t
                        loading.value = false
                    }

                    override fun onError(e: Throwable) {
                        isError.value = true
                        loading.value = true
                    }
                })
        )
    }


}



