package mobile.moviecatalog.android

object Constants {

    const val BASE_URL = "https://api.themoviedb.org/3/"
    const val API_KEY="90362210e9c76a29043c1e5f81596c56"

    const val KEY_WATCHLIST="WatchList"
    const val KEY_FAVORITES="Favorites"
    const val KEY_SEARCH="Search"
}