package mobile.moviecatalog.android

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.RecyclerView
import mobile.moviecatalog.android.databinding.ItemMovieBinding
import mobile.moviecatalog.android.model.MovieModel
import mobile.moviecatalog.android.model.Result
import mobile.moviecatalog.android.view.SearchFragmentDirections
import mobile.moviecatalog.android.view.WatchlistFragmentDirections


class MovieAdapter(list: ArrayList<Result>) :
    RecyclerView.Adapter<MovieAdapter.ViewHolder>() {
    private lateinit var databinding: ItemMovieBinding
    private var movieList: ArrayList<Result> = list
    private var counter = 0
    private var type = ""
    private lateinit var detailCallback:DetailCallback

    interface DetailCallback{
        fun goFragment(item:Result)

    }

    class ViewHolder(
        private val databinding: ItemMovieBinding,
        private val movieList: ArrayList<Result>
    ) : RecyclerView.ViewHolder(databinding.root) {

        val cardview = databinding.itemCv as CardView


        fun bind(list: ArrayList<Result>) {
            databinding.model = list.get(adapterPosition)
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        databinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_movie, parent, false)
        return ViewHolder(databinding, movieList)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(movieList)

        holder.cardview.setOnClickListener {

            detailCallback.goFragment(movieList.get(position))

        }
    }

    override fun getItemCount(): Int {
        return movieList.size
    }

    fun setMovieList(list: ArrayList<Result>) {
        this.movieList.addAll(list)
        notifyDataSetChanged()

    }


    fun setSearchList(list: ArrayList<Result>) {
        movieList = list
        notifyDataSetChanged()

    }

    fun clearList() {
        this.movieList.clear()
        notifyDataSetChanged()

    }

    fun setCallback(callback: DetailCallback){
         detailCallback=callback
    }

}