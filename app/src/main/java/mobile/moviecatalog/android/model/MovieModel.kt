package mobile.moviecatalog.android.model

import com.google.gson.annotations.SerializedName


data class MovieModel(
    @SerializedName("page")
    var page: Int? = null,
    @SerializedName("results")
    var results: ArrayList<Result>? = null,
    @SerializedName("total_results")
    var total_results: Int? = null,
    @SerializedName("total_pages")
    var total_pages: Int? = null

)