package mobile.moviecatalog.android.model

import com.google.gson.annotations.SerializedName

class RequestTokenResponse(

    @SerializedName("success")
    var success: String? = null,
    @SerializedName("expires_at")
    var expires_at: String? = null,
    @SerializedName("request_token")
    var request_token: String? = null


) {
}