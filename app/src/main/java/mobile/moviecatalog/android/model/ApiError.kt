package mobile.moviecatalog.android.model

import com.google.gson.annotations.SerializedName

class ApiError(
@SerializedName("status_message")
    var status_message: String? = null,
@SerializedName("status_code")
    var status_code: String? = null

) {
}