package mobile.moviecatalog.android.model

import com.google.gson.annotations.SerializedName

data class Configuration(
    @SerializedName("change_keys")
    var changeKeys: List<String>? = null,
    @SerializedName("images")
    var images: Images? = null
)