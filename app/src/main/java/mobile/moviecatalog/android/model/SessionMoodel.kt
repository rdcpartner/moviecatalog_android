package mobile.moviecatalog.android.model


import com.google.gson.annotations.SerializedName


data class SessionMoodel(
    @SerializedName("session_id")
    var sessionId: String? = null,
    @SerializedName("success")
    var success: Boolean? = null
)