package mobile.moviecatalog.android.model


import com.google.gson.annotations.SerializedName

data class Gravatar(
    @SerializedName("hash")
    var hash: String? = null
)