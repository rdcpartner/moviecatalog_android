package mobile.moviecatalog.android.model


import com.google.gson.annotations.SerializedName

data class Avatar(
    @SerializedName("gravatar")
    var gravatar: Gravatar? = null
)