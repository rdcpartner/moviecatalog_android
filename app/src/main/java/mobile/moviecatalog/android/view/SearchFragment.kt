package mobile.moviecatalog.android.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_search.*
import mobile.moviecatalog.android.Constants
import mobile.moviecatalog.android.MovieAdapter
import mobile.moviecatalog.android.R
import mobile.moviecatalog.android.databinding.FragmentSearchBinding
import mobile.moviecatalog.android.model.MovieModel
import mobile.moviecatalog.android.model.Result
import mobile.moviecatalog.android.viewmodel.SearchViewModel

class SearchFragment : Fragment(), MovieAdapter.DetailCallback {

    private lateinit var databinding: FragmentSearchBinding
    private lateinit var searchViewModel: SearchViewModel
    private lateinit var movieAdapter: MovieAdapter
    private lateinit var layoutManager: LinearLayoutManager
    private var tempMovieList: ArrayList<Result> = arrayListOf()
    private var isLoading = false
    private var isSearchClick = false
    var movieModel = MovieModel()

    var TOTAL_SIZE = 0
    var TOTAL_PAGE = 1
    var CURRENT_PAGE = 1
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        databinding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false)
        return databinding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        movieAdapter.clearList()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        TOTAL_SIZE = 0
        TOTAL_PAGE = 1
        CURRENT_PAGE = 1
        movieAdapter = MovieAdapter(ArrayList())
        movieAdapter.setCallback(this)
        layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        databinding.searchRv.layoutManager = layoutManager
        databinding.searchRv.adapter = movieAdapter

        searchViewModel =
            ViewModelProvider.AndroidViewModelFactory.getInstance(requireActivity().application)
                .create(SearchViewModel::class.java)
        searchViewModel.movieService(null)
        observeLiveData()
        pagination()
        setSearcher()
    }

    private fun observeLiveData() {
        searchViewModel.loading.observe(viewLifecycleOwner, Observer {
            if (it) {
                progressBar.visibility = View.VISIBLE
                databinding.searchRv.visibility = View.GONE
                databinding.errorLay.visibility = View.GONE
            } else {
                progressBar.visibility = View.GONE
                databinding.searchRv.visibility = View.VISIBLE
                databinding.errorLay.visibility = View.GONE
            }

        })

        searchViewModel.responseMovieModel.observe(viewLifecycleOwner, Observer {
            movieModel = it
            if (movieModel.results != null) {
                movieAdapter.setMovieList(movieModel.results!!)
                tempMovieList.addAll(movieModel.results!!)
                TOTAL_SIZE = TOTAL_SIZE + movieModel.results!!.size
                TOTAL_PAGE = movieModel.total_pages!!
                CURRENT_PAGE++
                databinding.progressBar.visibility = View.GONE
                isLoading = false
            }

        })
    }

    private fun pagination() {
        databinding.searchRv.addOnScrollListener(object : RecyclerView.OnScrollListener() {


            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                if (dy > 0) {
                    var vItem = layoutManager.childCount
                    var lItem = layoutManager.findFirstCompletelyVisibleItemPosition()
                    var count = movieAdapter.itemCount

                    if (!isLoading && vItem + lItem >= count) {
                        if (CURRENT_PAGE <= TOTAL_PAGE) {
                            databinding.progressBar.visibility = View.VISIBLE
                            searchViewModel.movieService(CURRENT_PAGE)
                            isLoading = true
                        }
                    }
                }

            }
        })
    }

    private fun setSearcher() {


        databinding.searcher.setOnQueryTextFocusChangeListener { v, hasFocus ->
            if (hasFocus)
                isSearchClick = true
        }
        databinding.searcher.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (isSearchClick == true) {
                    searchOperation(query)

                }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {

                if (isSearchClick == true) {
                    searchOperation(newText)
                }
                return true
            }
        })
    }

    private fun searchOperation(item: String?) {
        var searchList = arrayListOf<Result>()

        if (item.isNullOrEmpty()) {
            searchList = tempMovieList
        } else {
            tempMovieList.forEach {
                if (it.originalTitle!!.toString().contains(item!!, ignoreCase = true)) {
                    searchList.add(it)
                }
            }
        }
        if (searchList.size == 0) {
            databinding.errorLay.visibility = View.VISIBLE
            databinding.searchRv.visibility = View.GONE
        } else {
            databinding.errorLay.visibility = View.GONE
            databinding.searchRv.visibility = View.VISIBLE
            searchList.distinct()
            movieAdapter.setSearchList(searchList)

        }

    }

    override fun goFragment(item: Result) {
        var directions = SearchFragmentDirections.toDetail(item)
        findNavController().navigate(directions)
    }

}