package mobile.moviecatalog.android.view
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import mobile.moviecatalog.android.R


class MainActivity : AppCompatActivity(), LoginFragment.LoginCallback {

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
        findViewById<BottomNavigationView>(R.id.bottom_navigation)
            .setupWithNavController(navController)
        val loginFragment = navHostFragment.childFragmentManager.fragments[0] as LoginFragment;
        loginFragment.setLoginCallback(this)




          navController.addOnDestinationChangedListener { controller, destination, arguments -> if(destination.label=="Detail") bottom_navigation.visibility=View.GONE else bottom_navigation.visibility=View.VISIBLE }
        bottom_navigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.navigation_search -> {
                    navController.navigate(R.id.searchFragment)
                    bottom_navigation.selectedItemId=R.id.searchFragment
                }
                R.id.navigation_watchlist ->{
                    navController.navigate(R.id.watchlistFragment)
                    bottom_navigation.selectedItemId=R.id.watchlistFragment
                }
                R.id.navigation_favorites -> {
                    navController.navigate(R.id.favoritesFragment)
                    bottom_navigation.selectedItemId=R.id.favoritesFragment

                }
            }
            return@setOnNavigationItemSelectedListener true
        }
    }

    override fun isLoginSuccess(value: Boolean) {
        if (value) {
            menuLay.visibility = View.VISIBLE
            navController.navigate(R.id.login_to_search)
        }
    }
}