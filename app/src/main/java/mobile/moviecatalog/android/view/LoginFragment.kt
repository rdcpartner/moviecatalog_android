package mobile.moviecatalog.android.view

import android.animation.ValueAnimator
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import mobile.moviecatalog.android.R
import mobile.moviecatalog.android.databinding.FragmentLoginBinding
import mobile.moviecatalog.android.model.LoginRequest
import mobile.moviecatalog.android.viewmodel.LoginViewModel


class LoginFragment : Fragment() {
    private lateinit var loginViewModel: LoginViewModel
    private lateinit var dataBinding: FragmentLoginBinding
    private lateinit var loginCallback: LoginCallback

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        return dataBinding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loginViewModel =
            ViewModelProvider.AndroidViewModelFactory.getInstance(requireActivity().application)
                .create(LoginViewModel::class.java)
        actions()
        observeLoginResponse()
    }

    private fun actions() {
        dataBinding.loginButtonTv.setOnClickListener {

            minimizeButton(dataBinding.loginButtonTv, dataBinding.progressBar)
            val user = LoginRequest(
                dataBinding.userEt.text.toString(),
                dataBinding.passwordEt.text.toString(),
                null
            )
            loginViewModel.login(user)
        }
        dataBinding.userEt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) dataBinding.errorTv.visibility = View.GONE
        }
        dataBinding.passwordEt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) dataBinding.errorTv.visibility = View.GONE
        }


    }

    private fun observeLoginResponse() {
        loginViewModel.errorMessage.observe(viewLifecycleOwner, Observer {
            //error is true
            if (!it.isNullOrEmpty()) {
                dataBinding.errorTv.visibility = View.VISIBLE
                dataBinding.errorTv.text = loginViewModel.errorMessage.value
                enlargeButton(dataBinding.loginButtonTv, dataBinding.progressBar)
            } else
                dataBinding.errorTv.visibility = View.GONE
        })


        loginViewModel.isError.observe(viewLifecycleOwner, Observer {
            //error is false
            if (!it) {

                loginCallback.isLoginSuccess(true)

            }
        })

    }



    interface LoginCallback{
       fun isLoginSuccess(value:Boolean)
    }

    fun setLoginCallback(callback:LoginCallback){
        loginCallback=callback

    }
    fun animate(v: View, END_WIDTH: Int, DURATION: Int) {
        val anim = ValueAnimator.ofInt(v.measuredWidth, END_WIDTH)
        anim.addUpdateListener { valueAnimator ->
            val `val` = valueAnimator.animatedValue as Int
            if (`val` > 50) {
                val layoutParams = v.layoutParams
                layoutParams.width = `val`
                v.layoutParams = layoutParams
            } else v.visibility = View.INVISIBLE
        }
        anim.duration = DURATION.toLong()
        anim.start()
    }

    fun minimizeButton(view: TextView, progressBar: ProgressBar) {
        view.setText("");
        animate(view, 0, 300);
        progressBar.getIndeterminateDrawable().setColorFilter(
            Color.WHITE, android.graphics.PorterDuff.Mode.SRC_IN
        );
        progressBar.setVisibility(View.VISIBLE);

    }


    fun enlargeButton(view: TextView, progressBar: ProgressBar) {
        view.setVisibility(View.VISIBLE);
        animate(view, dataBinding.loginLay.width, 300);
        Handler(Looper.getMainLooper()).postDelayed({
            view.setText("LOGIN");
            progressBar.setVisibility(View.GONE);


        }, 300)


    }


}