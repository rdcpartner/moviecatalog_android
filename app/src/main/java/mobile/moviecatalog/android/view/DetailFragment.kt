package mobile.moviecatalog.android.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import kotlinx.coroutines.flow.callbackFlow
import mobile.moviecatalog.android.R
import mobile.moviecatalog.android.ScreenUtils
import mobile.moviecatalog.android.databinding.FragmentDetailBinding
import mobile.moviecatalog.android.model.Result


class DetailFragment : Fragment() {


    private lateinit var  binding:FragmentDetailBinding
    private val args: DetailFragmentArgs by navArgs()
    private lateinit var movieItem: Result


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding=DataBindingUtil.inflate(inflater, R.layout.fragment_detail, container, false)

        movieItem=args.İtemData
        return binding.root
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.model=movieItem
        binding.detailIv.layoutParams.height=ScreenUtils.getHeight(requireContext())/2
        binding.detailTv.text = movieItem.overview
        binding.starTv.text=movieItem.voteAverage.toString()
        binding.headerTv.text=movieItem.originalTitle
        binding.backIv.setOnClickListener {
            findNavController().navigateUp()


        }

    }


}