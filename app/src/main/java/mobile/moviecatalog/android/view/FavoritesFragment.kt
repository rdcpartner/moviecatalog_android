package mobile.moviecatalog.android.view

import android.graphics.Path
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_search.*
import mobile.moviecatalog.android.App
import mobile.moviecatalog.android.Constants
import mobile.moviecatalog.android.MovieAdapter
import mobile.moviecatalog.android.R
import mobile.moviecatalog.android.databinding.FragmentFavoritesBinding
import mobile.moviecatalog.android.databinding.FragmentWatchlistBinding
import mobile.moviecatalog.android.model.MovieModel
import mobile.moviecatalog.android.model.Result
import mobile.moviecatalog.android.model.SessionMoodel
import mobile.moviecatalog.android.viewmodel.LoginViewModel
import mobile.moviecatalog.android.viewmodel.WatchlistViewModel


class FavoritesFragment : Fragment(),MovieAdapter.DetailCallback {
    private lateinit var databinding:FragmentFavoritesBinding
    private lateinit var  watchlistViewModel:WatchlistViewModel
    private lateinit var movieAdapter: MovieAdapter
    private lateinit var layoutManager: LinearLayoutManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        databinding = DataBindingUtil.inflate(inflater, R.layout.fragment_favorites, container, false)
        return databinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        movieAdapter = MovieAdapter(ArrayList())
        movieAdapter.setCallback(this)
        layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        databinding.favListRv.layoutManager = layoutManager
        databinding.favListRv.adapter=movieAdapter

        watchlistViewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(requireActivity().application)
            .create(WatchlistViewModel::class.java)

        watchlistViewModel.setTypeForService(Constants.KEY_FAVORITES)

        if(!App.sessionId.isNullOrEmpty() && App.accountId!=null)
            watchlistViewModel.getFavList()

        else
            watchlistViewModel.getSessionID()

        observeService()

    }


    fun observeService(){
        watchlistViewModel.loading.observe(viewLifecycleOwner, Observer {
            if (it) {
                progressBar.visibility = View.VISIBLE
                databinding.favListRv.visibility=View.GONE

            } else {
                progressBar.visibility = View.GONE
                databinding.favListRv.visibility = View.VISIBLE

            }

        })





        watchlistViewModel.responseFavList.observe(viewLifecycleOwner, Observer {
            if(it!=null){
                movieAdapter.setMovieList(it.results!!)
            }

        })


    }

    override fun goFragment(item: Result) {
        var directions =FavoritesFragmentDirections .toDetail(item)
        findNavController().navigate(directions)
    }


}