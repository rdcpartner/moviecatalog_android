package mobile.moviecatalog.android

import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

@BindingAdapter("android:imageUrl")
fun imageUrl(view: ImageView, url: String) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null && url != null) {
        view.visibility=View.VISIBLE
        if (url.isNullOrEmpty())
        //view.setImageResource(R.drawable.no_image)
        else{
           var imagePath = App.configuration?.images?.secureBaseUrl+App.configuration?.images?.logoSizes?.get(5)+url
            Glide
                .with(parentActivity)
                .load(imagePath)

                .into(view);

        }
    }

}

