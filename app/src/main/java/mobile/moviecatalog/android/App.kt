package mobile.moviecatalog.android

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import mobile.moviecatalog.android.model.Configuration
import mobile.moviecatalog.android.model.LoginRequest

class App :Application(){


    companion object{

        var configuration:Configuration?=null

        var requestToken:String?=null
        var sessionId:String?=null
        var accountId:Int?=null

    }
    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}