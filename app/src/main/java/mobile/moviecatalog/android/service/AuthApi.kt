package mobile.moviecatalog.android.service

import io.reactivex.Single
import mobile.moviecatalog.android.model.Configuration
import mobile.moviecatalog.android.model.RequestTokenResponse
import retrofit2.http.*


interface AuthApi {

 @GET("authentication/token/new?")
 fun createRequestToken(@Query("api_key") api_key: String): Single<RequestTokenResponse>

 @POST("authentication/token/validate_with_login?")
 fun login(@Query("api_key") api_key: String,@Query( "username") username:String,@Query( "password") password:String,@Query( "request_token") request_token:String  ): Single<RequestTokenResponse>


 @GET("configuration")
 fun getConfigs(@Query("api_key") api_key: String): Single<Configuration>

}