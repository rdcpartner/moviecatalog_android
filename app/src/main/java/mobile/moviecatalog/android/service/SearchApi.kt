package mobile.moviecatalog.android.service

import io.reactivex.Single
import mobile.moviecatalog.android.model.MovieModel
import mobile.moviecatalog.android.model.RequestTokenResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchApi {

    @GET("discover/movie")
    fun getAllMovies(@Query("api_key") api_key: String): Single<MovieModel>

    @GET("discover/movie")
    fun getMoviesByPage(@Query("api_key") api_key: String, @Query("page") page: Int): Single<MovieModel>

}