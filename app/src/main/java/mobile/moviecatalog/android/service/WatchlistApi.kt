package mobile.moviecatalog.android.service

import io.reactivex.Single
import mobile.moviecatalog.android.model.AccountModel
import mobile.moviecatalog.android.model.MovieModel
import mobile.moviecatalog.android.model.Result
import mobile.moviecatalog.android.model.SessionMoodel
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface WatchlistApi {



    @GET("account")
    fun getAccountId(@Query("api_key") api_key: String,@Query("session_id") session_id: String): Single<AccountModel>


    @POST("authentication/session/new")
    fun getSessionToken(@Query("api_key") api_key: String,@Query("request_token") request_token: String): Single<SessionMoodel>


    @GET("account/{account_id}/watchlist/movies?")
    fun getWatchlist(@Path("account_id") account_id:Int,@Query("api_key") api_key: String, @Query("session_id") session_id: String): Single<MovieModel>

    @GET("account/{account_id}/favorite/movies?")
    fun getFavorites(@Path("account_id") account_id:Int,@Query("api_key") api_key: String, @Query("session_id") session_id: String): Single<MovieModel>

}