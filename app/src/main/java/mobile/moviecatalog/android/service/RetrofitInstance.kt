package mobile.moviecatalog.android.service

import mobile.moviecatalog.android.Constants
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitInstance {


   fun getRetrofit():Retrofit{
          return   Retrofit.Builder()
               .baseUrl(Constants.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
               .addConverterFactory(GsonConverterFactory.create())
               .build()
   }



}